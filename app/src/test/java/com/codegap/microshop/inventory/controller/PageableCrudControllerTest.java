package com.codegap.microshop.inventory.controller;

import com.codegap.microshop.inventory.dto.PagedListDto;
import com.codegap.microshop.inventory.model.IdentifiableEntity;
import com.codegap.microshop.inventory.service.PageableCrudService;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import jakarta.inject.Inject;
import lombok.Getter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public abstract class PageableCrudControllerTest<T extends IdentifiableEntity> {

    @Inject
    @Client("/")
    @Getter
    private HttpClient client;

    @Getter
    private final String collectionUri;

    // Micronaut's HttpClient needs a class to convert the response to, and due to
    // the non-reflective nature of the framework, we cannot infer the type arguments.
    // Thus we have to resort to this.
    @Getter
    private final Class<T> singleClass;
    @Getter
    private final Class<? extends PagedListDto<T>> listClass;

    // injected behaviour
    private final Supplier<List<T>> entitiesSupplier;
    private final BiFunction<T, Object, T> modifiedCopyBiFunction;
    private final BiConsumer<T, T> verifyEntityBiConsumer;

    // populated with setupEntities(), which is called before each test
    private List<T> entities;

    public PageableCrudControllerTest(String collectionUri,
                                          Class<T> singleClass,
                                          Class<? extends PagedListDto<T>> listClass,
                                          Supplier<List<T>> entitiesSupplier,
                                          BiFunction<T, Object, T> modifiedCopyBiFunction,
                                          BiConsumer<T, T> verifyEntityBiConsumer) {
        this.collectionUri = collectionUri;
        this.singleClass = singleClass;
        this.listClass = listClass;
        this.entitiesSupplier = entitiesSupplier;
        this.modifiedCopyBiFunction = modifiedCopyBiFunction;
        this.verifyEntityBiConsumer = verifyEntityBiConsumer;
    }

    /**
     * Implement like so:
     *     \@Inject
     *     \@Getter
     *     BrandService service;
     */
    protected abstract PageableCrudService<T> getService();

    protected <C> HttpResponse<C> getResponse(HttpRequest<?> req, Class<C> responseClass) {
        return client.toBlocking().exchange(req, responseClass);
    }

    protected String getIdentityUri(T t) {
        return ApiUri.getIdentityUri(getCollectionUri(), t);
    }

    protected String getIdentityUri(Long id) {
        return ApiUri.getIdentityUri(getCollectionUri(), id);
    }

    protected String getBatchUri() {
        return ApiUri.getBatchUri(getCollectionUri());
    }

    protected List<T> getEntities() {
        if (entities == null) {
            initializeEntities();
        }
        return entities;
    }

    protected Pageable getDefaultPageable() {
        return Pageable.from(0, getEntities().size());
    }

    protected void initializeEntities() {
        entities = entitiesSupplier.get();
    }

    protected T createModifiedCopy(T t, Object modification) {
        return modifiedCopyBiFunction.apply(t, modification);
    }

    protected void verifyEntity(T expected, T actual) {
        verifyEntityBiConsumer.accept(expected, actual);
    }

    @BeforeEach
    public void setup() {
        initializeEntities();
    }

    @Test
    public void testFindAllNoResult() {
        when(getService().findAll(any(Pageable.class))).thenReturn(
                Page.of(List.of(), getDefaultPageable(), 0)
        );

        HttpResponse<? extends PagedListDto<T>> res = getResponse(
                HttpRequest.GET(getCollectionUri()), getListClass()
        );

        assertEquals(HttpStatus.OK, res.status());

        PagedListDto<T> body = res.body();
        assertNotNull(body);
        assertNull(body.getContent());

        assertEquals(0, body.getMeta().getItems());
        assertEquals(0, body.getMeta().getPages());
        assertEquals(1, body.getMeta().getCurrent());

        verify(getService(), times(1)).findAll(any(Pageable.class));
        verifyNoMoreInteractions(getService());
    }

    @Test
    public void testFindAll() {
        when(getService().findAll(any(Pageable.class))).thenReturn(
                Page.of(getEntities(), getDefaultPageable(), getEntities().size())
        );

        HttpResponse<? extends PagedListDto<T>> res = getResponse(
                HttpRequest.GET(getCollectionUri()), getListClass()
        );
        assertEquals(HttpStatus.OK, res.status());

        PagedListDto<T> body = res.body();
        assertNotNull(body);
        assertEquals(3, body.getContent().size());

        for (int i = 0; i < getEntities().size(); i++) {
            verifyEntity(getEntities().get(i), body.getContent().get(i));
        }

        assertEquals(3, body.getMeta().getItems());
        assertEquals(1, body.getMeta().getCurrent());
        assertEquals(1, body.getMeta().getPages());

        verify(getService(), times(1)).findAll(any(Pageable.class));
        verifyNoMoreInteractions(getService());
    }

    @Test
    public void testFindAllSearch() {
        final String SEARCH_ENCODED = "SEA+RCH";
        final String SEARCH_DECODED = "SEA RCH";

        final T entity = getEntities().get(0);

        when(getService().search(eq(SEARCH_DECODED), any(Pageable.class))).thenReturn(
                Page.of(List.of(entity), getDefaultPageable(), 1)
        );

        HttpResponse<? extends PagedListDto<T>> res = getResponse(
                HttpRequest.GET(getCollectionUri() + "?search=" + SEARCH_ENCODED), getListClass()
        );

        assertEquals(HttpStatus.OK, res.status());

        PagedListDto<T> body = res.body();
        assertNotNull(body);
        assertEquals(1, body.getContent().size());

        verifyEntity(entity, body.getContent().get(0));

        assertEquals(1, body.getMeta().getItems());
        assertEquals(1, body.getMeta().getCurrent());
        assertEquals(1, body.getMeta().getPages());

        verify(getService(), times(1)).search(eq(SEARCH_DECODED), any(Pageable.class));
        verifyNoMoreInteractions(getService());
    }

    @Test
    public void testFindAllPaged() {
        final T entity = getEntities().get(0);

        List<T> result = List.of(entity, getEntities().get(1));

        when(getService().findAll(any(Pageable.class))).thenReturn(
                Page.of(result, Pageable.from(0, result.size()), result.size())
        );

        HttpResponse<? extends PagedListDto<T>> res = getResponse(
                HttpRequest.GET(getCollectionUri()), getListClass()
        );

        assertEquals(HttpStatus.OK, res.status());

        PagedListDto<T> body = res.body();
        assertNotNull(body);
        assertEquals(result.size(), body.getContent().size());

        verifyEntity(entity, body.getContent().get(0));
        verifyEntity(getEntities().get(1), body.getContent().get(1));

        assertEquals(result.size(), body.getMeta().getItems());
        assertEquals(1, body.getMeta().getCurrent());
        assertEquals(1, body.getMeta().getPages());

        verify(getService(), times(1)).findAll(any(Pageable.class));
        verifyNoMoreInteractions(getService());
    }

    @Test
    public void testFindById() {
        final T entity = getEntities().get(0);

        when(getService().findById(eq(entity.getId()))).thenReturn(
                Optional.of(entity)
        );

        HttpResponse<T> res = getResponse(
                HttpRequest.GET(getIdentityUri(entity.getId())), getSingleClass()
        );

        assertEquals(HttpStatus.OK, res.status());

        T body = res.body();
        assertNotNull(body);

        verifyEntity(entity, body);

        verify(getService(), times(1)).findById(anyLong());
        verifyNoMoreInteractions(getService());
    }

    @Test
    void testFindByIdNotFound() {
        final Long UNKNOWN_ID = 999L;
        when(getService().findById(eq(UNKNOWN_ID))).thenReturn(
                Optional.empty()
        );

        HttpClientResponseException exception =
                assertThrows(HttpClientResponseException.class, () -> getResponse(
                        HttpRequest.GET(getIdentityUri(UNKNOWN_ID)), getSingleClass()
                ));

        assertEquals(HttpStatus.NOT_FOUND, exception.getStatus());

        verify(getService(), times(1)).findById(eq(UNKNOWN_ID));
        verifyNoMoreInteractions(getService());
    }


    @Test
    void testCreate() {
        final T entity = getEntities().get(0);

        T NEW_ENTITY = createModifiedCopy(entity, "NEW").setId(null);
        T SAVED_ENTITY = createModifiedCopy(entity, "NEW");

        when(getService().insert(any())).thenReturn(
                SAVED_ENTITY
        );

        HttpResponse<T> res = getResponse(
                HttpRequest.POST(getCollectionUri(), NEW_ENTITY), getSingleClass()
        );

        assertEquals(HttpStatus.CREATED, res.status());

        T body = res.body();
        assertNotNull(body);

        verifyEntity(SAVED_ENTITY, body);

        String location = res.header("location");
        assertNotNull(location);
        assertTrue(location.endsWith(getIdentityUri(SAVED_ENTITY)));

        verify(getService(), times(1)).insert(any());
        verifyNoMoreInteractions(getService());
    }

    @Test
    void testCreateBatch() {
        HttpRequest<List<T>> req = HttpRequest.POST(getBatchUri(), getEntities());
        HttpResponse<Void> res = getClient().toBlocking().exchange(req);

        assertEquals(HttpStatus.ACCEPTED, res.status());

        verify(getService(), times(1)).insertAll(any());
        verifyNoMoreInteractions(getService());
    }

    @Test
    public void testUpdate() {
        final T entity = getEntities().get(0);

        T UPDATED_ENTITY = createModifiedCopy(entity, "MODIFIED");

        when(getService().update(eq(entity.getId()), any())).thenReturn(
                Optional.of(UPDATED_ENTITY)
        );

        HttpResponse<T> res = getResponse(
                HttpRequest.PUT(getIdentityUri(entity.getId()), UPDATED_ENTITY), getSingleClass()
        );

        assertEquals(HttpStatus.OK, res.status());

        T body = res.body();
        assertNotNull(body);

        verifyEntity(UPDATED_ENTITY, body);

        verify(getService(), times(1)).update(eq(entity.getId()), any());
        verifyNoMoreInteractions(getService());
    }

    @Test
    public void testUpdateNotFound() {
        final Long UNKNOWN_ID = 999L;
        final T entity = getEntities().get(0);

        T UNKNOWN_ENTITY = createModifiedCopy(entity, "MODIFIED").setId(UNKNOWN_ID);

        when(getService().update(eq(UNKNOWN_ID), any())).thenReturn(
                Optional.empty()
        );

        HttpClientResponseException exception =
                assertThrows(HttpClientResponseException.class, () -> getResponse(
                        HttpRequest.PUT(getIdentityUri(UNKNOWN_ID), UNKNOWN_ENTITY), getSingleClass()
                ));

        assertEquals(HttpStatus.NOT_FOUND, exception.getStatus());

        verify(getService(), times(1)).update(eq(UNKNOWN_ID), any());
        verifyNoMoreInteractions(getService());
    }

    @Test
    public void testDelete() {
        final Long ID = 999L;
        HttpRequest<Void> req = HttpRequest.DELETE(getIdentityUri(ID));
        HttpResponse<Void> res = getClient().toBlocking().exchange(req);

        assertEquals(HttpStatus.NO_CONTENT, res.status());

        assertNull(res.body());

        verify(getService(), times(1)).deleteById(eq(ID));
        verifyNoMoreInteractions(getService());
    }

    @Test
    public void testDeleteBatch() {
        List<Long> IDS = getEntities().stream().map(IdentifiableEntity::getId).collect(Collectors.toList());

        HttpRequest<List<Long>> req = HttpRequest.DELETE(getBatchUri(), IDS);
        HttpResponse<Void> res = getClient().toBlocking().exchange(req);

        assertEquals(HttpStatus.ACCEPTED, res.status());

        assertNull(res.body());

        verify(getService(), times(1)).deleteAll(eq(IDS));
        verifyNoMoreInteractions(getService());
    }

    @Test
    public void testDeleteAll() {
        HttpRequest<Void> req = HttpRequest.DELETE(getCollectionUri());
        HttpResponse<Void> res = getClient().toBlocking().exchange(req);

        assertEquals(HttpStatus.ACCEPTED, res.status());

        assertNull(res.body());

        verify(getService(), times(1)).deleteAll();
        verifyNoMoreInteractions(getService());
    }
}
