package com.codegap.microshop.inventory.service.impl;

import com.codegap.microshop.inventory.dao.impl.BrandRepository;
import com.codegap.microshop.inventory.dao.impl.ProductRepository;
import com.codegap.microshop.inventory.dto.impl.BrandDetailsDto;
import com.codegap.microshop.inventory.model.impl.Brand;
import com.codegap.microshop.inventory.service.PageableCrudServiceTest;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import lombok.Getter;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@MicronautTest
public class BrandServiceTest extends PageableCrudServiceTest<Brand> {
    @Inject
    @Getter
    BrandService service;

    @Inject
    @Getter
    BrandRepository repository;

    @MockBean(BrandRepository.class)
    BrandRepository brandRepository() {
        return mock(BrandRepository.class);
    }

    @Inject
    ProductRepository productRepository;

    @MockBean(ProductRepository.class)
    ProductRepository productRepository() {
        return mock(ProductRepository.class);
    }

    public BrandServiceTest() {
        super(() -> List.of(
                        Brand.builder().id(1L).name("Brand 1").build(),
                        Brand.builder().id(2L).name("Brand 2").build(),
                        Brand.builder().id(3L).name("Brand 3").build()
                ),
                (brand, modification) -> brand.toBuilder()
                        .name(String.valueOf(modification))
                        .build(),
                (expected, actual) -> {
                    assertEquals(expected.getId(), actual.getId());
                    assertEquals(expected.getName(), actual.getName());
                }
        );
    }
    
    @Test
    // Optional<BrandDto> findDTOById(Long id);
    public void testFindDtoById() {
        final Brand entity = getEntities().get(0);
        final long ID = entity.getId();
        final int PRODUCT_COUNT = 5;

        when(getRepository().findById(eq(ID))).thenReturn(
                Optional.of(entity)
        );
        when(this.productRepository.countByBrand(eq(entity))).thenReturn(PRODUCT_COUNT);

        Optional<BrandDetailsDto> result = getService().findDetailsDtoById(ID);

        assertNotNull(result);
        assertTrue(result.isPresent());

        assertEquals(entity.getId(), result.get().getId());
        assertEquals(entity.getName(), result.get().getName());
        assertEquals(PRODUCT_COUNT, result.get().getProductCount());

        verify(getRepository(), times(1)).findById(eq(ID));
        verifyNoMoreInteractions(getRepository());
        verify(productRepository, times(1)).countByBrand(eq(entity));
        verifyNoMoreInteractions(productRepository);
    }

    @Test
    // Optional<BrandDto> findDTOById(Long id);
    public void testFindDtoByIdNotFound() {
        final long id = 999;

        when(getRepository().findById(eq(id))).thenReturn(
                Optional.empty()
        );

        Optional<BrandDetailsDto> result = getService().findDetailsDtoById(id);

        assertNotNull(result);
        assertTrue(result.isEmpty());

        verify(getRepository(), times(1)).findById(eq(id));
        verifyNoMoreInteractions(getRepository());

        verifyNoInteractions(productRepository);
    }
}
