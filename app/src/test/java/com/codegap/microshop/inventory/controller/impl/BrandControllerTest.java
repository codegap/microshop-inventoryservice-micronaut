package com.codegap.microshop.inventory.controller.impl;

import com.codegap.microshop.inventory.controller.ApiUri;
import com.codegap.microshop.inventory.controller.PageableCrudControllerTest;
import com.codegap.microshop.inventory.dto.impl.BrandDetailsDto;
import com.codegap.microshop.inventory.dto.impl.BrandPagedListDto;
import com.codegap.microshop.inventory.model.impl.Brand;
import com.codegap.microshop.inventory.service.impl.BrandService;
import io.micronaut.context.env.Environment;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@MicronautTest(environments = Environment.TEST)
@Slf4j
public class BrandControllerTest extends PageableCrudControllerTest<Brand> {

    @Inject
    @Getter
    BrandService service;

    @MockBean(BrandService.class)
    BrandService brandService() {
        return mock(BrandService.class);
    }

    public BrandControllerTest() {
        super(ApiUri.BRAND_V1, Brand.class, BrandPagedListDto.class,
                () -> List.of(
                        Brand.builder().id(1L).name("Brand 1").build(),
                        Brand.builder().id(2L).name("Brand 2").build(),
                        Brand.builder().id(3L).name("Brand 3").build()
                ),
                (brand, modification) -> brand.toBuilder()
                        .name(String.valueOf(modification))
                        .build(),
                (expected, actual) -> {
                    assertEquals(expected.getId(), actual.getId());
                    assertEquals(expected.getName(), actual.getName());
                }
        );
    }


    //// Custom tests

    @Test
    public void testFindDetailsDtoById() {
        final int PRODUCT_COUNT = 5;
        final Brand entity = getEntities().get(0);

        final String URL = getIdentityUri(entity.getId()) + ApiUri.VIEW_DETAILS;

        when(service.findDetailsDtoById(eq(entity.getId()))).thenReturn(
                Optional.of(BrandDetailsDto.of(entity, PRODUCT_COUNT))
        );

        HttpResponse<BrandDetailsDto> res = super.getResponse(
                HttpRequest.GET(URL),
                BrandDetailsDto.class
        );

        assertEquals(HttpStatus.OK, res.status());

        BrandDetailsDto body = res.body();
        assertNotNull(body);

        assertEquals(entity.getId(), body.getId());
        assertEquals(entity.getName(), body.getName());
        assertEquals(PRODUCT_COUNT, body.getProductCount());

        verify(service, times(1)).findDetailsDtoById(anyLong());
        verifyNoMoreInteractions(getService());
    }

    @Test
    void testFindDetailsDtoByIdNotFound() {
        final Long UNKNOWN_ID = 999L;
        final String URL = getIdentityUri(UNKNOWN_ID) + ApiUri.VIEW_DETAILS;

        when(service.findDetailsDtoById(eq(UNKNOWN_ID))).thenReturn(
                Optional.empty()
        );

        HttpClientResponseException exception =
                assertThrows(HttpClientResponseException.class, () -> super.getResponse(
                        HttpRequest.GET(URL),
                        BrandDetailsDto.class
                ));

        assertEquals(HttpStatus.NOT_FOUND, exception.getStatus());

        verify(service, times(1)).findDetailsDtoById(eq(UNKNOWN_ID));
        verifyNoMoreInteractions(getService());
    }
}
