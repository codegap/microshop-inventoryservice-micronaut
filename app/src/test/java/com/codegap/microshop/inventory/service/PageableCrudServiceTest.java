package com.codegap.microshop.inventory.service;

import com.codegap.microshop.inventory.model.IdentifiableEntity;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.repository.PageableRepository;
import lombok.Getter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.AdditionalAnswers;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public abstract class PageableCrudServiceTest<T extends IdentifiableEntity> {
    @Getter
    private final Supplier<List<T>> entitiesSupplier;
    @Getter
    private final BiFunction<T, Object, T> modifiedCopyBiFunction;
    @Getter
    private final BiConsumer<T, T> verifyEntityBiConsumer;

    // populated with setupEntities(), which is called before each test
    private List<T> entities;

    protected PageableCrudServiceTest(
            Supplier<List<T>> entitiesSupplier,
            BiFunction<T, Object, T> modifiedCopyBiFunction,
            BiConsumer<T, T> verifyEntityBiConsumer) {
        this.entitiesSupplier = entitiesSupplier;
        this.modifiedCopyBiFunction = modifiedCopyBiFunction;
        this.verifyEntityBiConsumer = verifyEntityBiConsumer;
    }

    protected List<T> getEntities() {
        if (entities == null) {
            setupEntities();
        }
        return entities;
    }

    protected void setupEntities() {
        entities = entitiesSupplier.get();
    }

    protected T createModifiedCopy(T t, Object modification) {
        return modifiedCopyBiFunction.apply(t, modification);
    }

    protected void verifyEntity(T expected, T actual) {
        verifyEntityBiConsumer.accept(expected, actual);
    }

    // Easily implemented by putting Lombok's @Getter on the main injected service called 'service'
    protected abstract PageableCrudService<T> getService();

    // Easily implemented by putting Lombok's @Getter on the main injected repository called 'repository'
    protected abstract PageableRepository<T, Long> getRepository();

    protected Pageable getDefaultPageable() {
        return Pageable.from(0, getEntities().size());
    }

    @BeforeEach
    public void setup() {
        setupEntities();
    }

    @Test
    // Page<T> findAll(Pageable pageable);
    public void testFindAll() {
        when(getRepository().findAll(any(Pageable.class))).thenReturn(
                Page.of(getEntities(), getDefaultPageable(), getEntities().size())
        );

        Page<T> result = getService().findAll(getDefaultPageable());

        assertNotNull(result);

        assertEquals(1, result.getTotalPages());
        assertEquals(getEntities().size(), result.getTotalSize());
        assertEquals(getEntities().size(), result.getNumberOfElements());
        assertEquals(0, result.getPageNumber());

        assertNotNull(result.getContent());
        assertEquals(getEntities().size(), result.getContent().size());

        for (int i = 0; i < getEntities().size(); i++) {
            verifyEntity(getEntities().get(i), result.getContent().get(i));
        }

        verify(getRepository(), times(1)).findAll(any(Pageable.class));
        verifyNoMoreInteractions(getRepository());
    }

    @Test
    // Page<T> findAll(Pageable pageable);
    public void testFindAllNotFound() {
        when(getRepository().findAll(any(Pageable.class))).thenReturn(
                Page.of(List.of(), getDefaultPageable(), 0)
        );

        Page<T> result = getService().findAll(getDefaultPageable());

        assertNotNull(result);

        assertEquals(0, result.getTotalPages());
        assertEquals(0, result.getTotalSize());
        assertEquals(0, result.getNumberOfElements());
        assertEquals(0, result.getPageNumber());

        assertNotNull(result.getContent());
        assertEquals(0, result.getContent().size());

        verify(getRepository(), times(1)).findAll(any(Pageable.class));
        verifyNoMoreInteractions(getRepository());
    }

    @Test
    @SuppressWarnings("unchecked")
    // Page<T> search(String search, Pageable pageable);
    public void testSearch() {
        final String SEARCH = "SEARCH";
        final Pageable requestedPageable = getDefaultPageable();
        final T ENTITY = getEntities().get(0);

        // avoiding the call to repository::findTbyXYZ by effectively replacing the
        // searchhandler with the disabled default instance
        PageableCrudService<T> spiedService = Mockito.spy(getService());
        PageableSearchHandler<T> mockedSearchHandler = Mockito.mock(PageableSearchHandler.class);
        when(spiedService.getSearchHandler()).thenReturn(
                mockedSearchHandler
        );
        when(mockedSearchHandler.handleOrElse(eq(SEARCH), eq(requestedPageable), any(Function.class))).thenReturn(
                // mimic the search output
                Page.of(List.of(ENTITY), Pageable.from(0, 1), 1)
        );

        Page<T> result = spiedService.search(SEARCH, requestedPageable);

        assertNotNull(result);

        assertEquals(1, result.getTotalPages());
        assertEquals(1, result.getTotalSize());
        assertEquals(1, result.getNumberOfElements());
        assertEquals(0, result.getPageNumber());

        assertNotNull(result.getContent());
        assertEquals(1, result.getContent().size());
        verifyEntity(ENTITY, result.getContent().get(0));

        verify(spiedService, times(1)).search(eq(SEARCH), eq(requestedPageable));
        verify(spiedService, times(1)).getSearchHandler();
        verifyNoMoreInteractions(spiedService);

        verify(mockedSearchHandler, times(1)).handleOrElse(eq(SEARCH), eq(requestedPageable), any(Function.class));
        verifyNoMoreInteractions(mockedSearchHandler);

        verifyNoMoreInteractions(getRepository());
    }

    @Test
    @SuppressWarnings("unchecked")
    // Page<T> search(String search, Pageable pageable);
    public void testSearchNotFound() {
        final String SEARCH = "SEARCH";
        final Pageable requestedPageable = getDefaultPageable();

        // avoiding the call to repository::findTbyXYZ by effectively replacing the
        // searchhandler with the disabled default instance
        PageableCrudService<T> spiedService = Mockito.spy(getService());
        PageableSearchHandler<T> mockedSearchHandler = Mockito.mock(PageableSearchHandler.class);
        when(spiedService.getSearchHandler()).thenReturn(
                mockedSearchHandler
        );
        when(mockedSearchHandler.handleOrElse(eq(SEARCH), eq(requestedPageable), any(Function.class))).thenReturn(
                // mimic the (empty) search output
                Page.of(List.of(), getDefaultPageable(), 0)
        );

        Page<T> result = spiedService.search(SEARCH, requestedPageable);

        assertNotNull(result);

        assertEquals(0, result.getTotalPages());
        assertEquals(0, result.getTotalSize());
        assertEquals(0, result.getNumberOfElements());
        assertEquals(0, result.getPageNumber());

        assertNotNull(result.getContent());
        assertEquals(0, result.getContent().size());

        verify(spiedService, times(1)).search(eq(SEARCH), eq(requestedPageable));
        verify(spiedService, times(1)).getSearchHandler();
        verifyNoMoreInteractions(spiedService);

        verify(mockedSearchHandler, times(1)).handleOrElse(eq(SEARCH), eq(requestedPageable), any(Function.class));
        verifyNoMoreInteractions(mockedSearchHandler);

        verifyNoMoreInteractions(getRepository());
    }

    @Test
    // Optional<T> findById(Long id);
    public void testFindById() {
        final T ENTITY = getEntities().get(0);
        final long ID = ENTITY.getId();

        when(getRepository().findById(eq(ID))).thenReturn(
                Optional.of(ENTITY)
        );

        Optional<T> result = getService().findById(ID);

        assertNotNull(result);
        assertTrue(result.isPresent());

        verifyEntity(ENTITY, result.get());

        verify(getRepository(), times(1)).findById(eq(ID));
        verifyNoMoreInteractions(getRepository());
    }

    @Test
    // Optional<T> findById(Long id);
    public void testFindByIdNotFound() {
        final long ID = 999;
        when(getRepository().findById(eq(ID))).thenReturn(
                Optional.empty()
        );

        Optional<T> result = getService().findById(ID);

        assertNotNull(result);
        assertTrue(result.isEmpty());

        verify(getRepository(), times(1)).findById(eq(ID));
        verifyNoMoreInteractions(getRepository());
    }

    @Test
    // T insert(T t);
    public void testInsert() {
        final T ENTITY = getEntities().get(0);
        final T INSERT = createModifiedCopy(ENTITY, "INSERT").setId(null);

        when(getRepository().save(eq(INSERT))).thenReturn(
                ENTITY
        );

        T result = getService().insert(INSERT);

        assertNotNull(result);

        verifyEntity(ENTITY, result);

        verify(getRepository(), times(1)).save(eq(INSERT));
        verifyNoMoreInteractions(getRepository());
    }

    @Test
    // void insertAll(List<T> list);
    public void testInsertAll() {
        final T ENTITY = getEntities().get(0);
        final List<T> INSERTABLE_COPIES = getEntities().stream()
                .map(e -> createModifiedCopy(e, "INSERT").<T>setId(null))
                .collect(Collectors.toList());

        when(getRepository().save(any())).thenReturn(
            ENTITY // dummy return value with non-null id
        );

        getService().insertAll(INSERTABLE_COPIES);

        verify(getRepository(), times(getEntities().size())).save(any());
        verifyNoMoreInteractions(getRepository());
    }

    @Test
    // Optional<T> update(Long id, T t);
    public void testUpdate() {
        final T ENTITY = getEntities().get(0);
        final T EXISTING = createModifiedCopy(ENTITY, "EXISTING");
        final T UPDATE = createModifiedCopy(EXISTING, "UPDATE");

        when(getRepository().findById(eq(EXISTING.getId()))).thenReturn(
                Optional.of(EXISTING)
        );
        when(getRepository().update(any())).then(
                AdditionalAnswers.returnsFirstArg()
        );

        Optional<T> result = getService().update(EXISTING.getId(), UPDATE);

        assertNotNull(result);
        assertTrue(result.isPresent());

        verifyEntity(UPDATE, result.get());

        verify(getRepository(), times(1)).findById(eq(EXISTING.getId()));
        verify(getRepository(), times(1)).update(any());
        verifyNoMoreInteractions(getRepository());
    }

    @Test
    // Optional<T> update(Long id, T t);
    public void testUpdateNotFound() {
        final Long ID = 999L;
        final T ENTITY = getEntities().get(0);
        final T UPDATE = createModifiedCopy(ENTITY, "UPDATE");

        when(getRepository().findById(any())).thenReturn(
                Optional.empty()
        );

        Optional<T> result = getService().update(ID, UPDATE);

        assertNotNull(result);
        assertTrue(result.isEmpty());

        verify(getRepository(), times(1)).findById(eq(ID));
        verifyNoMoreInteractions(getRepository());
    }

    @Test
    // void deleteById(Long id);
    public void testDeleteById() {
        final T ENTITY = getEntities().get(0);
        final long ID = ENTITY.getId();

        getService().deleteById(ID);

        verify(getRepository(), times(1)).deleteById(eq(ID));
        verifyNoMoreInteractions(getRepository());
    }

    @Test
    // void deleteAll(List<Long> ids);
    public void testDeleteAllList() {
        final List<Long> IDS = List.of(1L, 2L, 3L);

        getService().deleteAll(IDS);

        verify(getRepository(), times(IDS.size())).deleteById(anyLong());
        verifyNoMoreInteractions(getRepository());
    }

    @Test
    // void deleteAll();
    public void testDeleteAll() {
        getService().deleteAll();

        verify(getRepository(), times(1)).deleteAll();
        verifyNoMoreInteractions(getRepository());
    }
}
