package com.codegap.microshop.inventory.model;


public interface IdentifiableEntity {
    Long getId();
    <T extends IdentifiableEntity> T setId(Long id);
}
