package com.codegap.microshop.inventory.model.impl;

import com.codegap.microshop.inventory.model.IdentifiableEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.micronaut.core.annotation.NonNull;
import io.micronaut.data.model.Sort;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors(chain = true)
@Builder(toBuilder = true)
public class Brand implements IdentifiableEntity {
    public static final Sort SORT = Sort.of(
            Sort.Order.asc("name")
    );

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 100)
    private String name;

    @OneToMany(mappedBy = "brand", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL, orphanRemoval = true)
    @ToString.Exclude
    @JsonIgnore // not returning the LAZY Product list when retrieving a Brand
    @Builder.Default // initialize the list when a Lombok builder is created
    private List<Product> products = new ArrayList<>();

    public void addProduct(Product product) {
        this.products.add(product);
        product.setBrand(this);
    }

    // Modifies the referenced existing object by overwriting the name field with
    // the value from the supplied merge object
    public static void merge(@NonNull Brand existing, @NonNull Brand merge) {
        existing.setName(merge.getName());
    }
}
