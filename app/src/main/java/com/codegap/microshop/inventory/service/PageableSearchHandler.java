package com.codegap.microshop.inventory.service;

import com.codegap.microshop.inventory.model.IdentifiableEntity;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;

import java.util.function.BiFunction;
import java.util.function.Function;

public class PageableSearchHandler<T> {
    private final BiFunction<String, Pageable, Page<T>> searchFunction;

    private PageableSearchHandler(BiFunction<String, Pageable, Page<T>> searchFunction) {
        this.searchFunction = searchFunction;
    }

    public Page<T> handleOrElse(String search, Pageable pageable, Function<Pageable, Page<T>> fallback) {
        return isEnabled() ? searchFunction.apply(search, pageable) : fallback.apply(pageable);
    }

    public boolean isEnabled() {
        return searchFunction != null;
    }

    public static <U extends IdentifiableEntity> PageableSearchHandler<U> disabled() {
        return new PageableSearchHandler<U>(null);
    }

    public static <U extends IdentifiableEntity> PageableSearchHandler<U> of(BiFunction<String, Pageable, Page<U>> searchFunction) {
        return new PageableSearchHandler<U>(searchFunction);
    }
}