package com.codegap.microshop.inventory.dao.impl;

import com.codegap.microshop.inventory.model.impl.Brand;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.repository.PageableRepository;

@Repository
public interface BrandRepository extends PageableRepository<Brand, Long> {
    Page<Brand> findAllByNameContains(String search, Pageable pageable);
}
