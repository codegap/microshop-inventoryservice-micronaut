package com.codegap.microshop.inventory.service;

import com.codegap.microshop.inventory.model.IdentifiableEntity;

import java.util.function.BiConsumer;

public class MergeHandler<T extends IdentifiableEntity> {
    private final BiConsumer<T, T> mergeConsumer;

    private MergeHandler(BiConsumer<T, T> mergeConsumer) {
        this.mergeConsumer = mergeConsumer;
    }

    public void handle(T existing, T merge) {
        mergeConsumer.accept(existing, merge);
    }

    public static <T1 extends IdentifiableEntity> MergeHandler<T1> of(BiConsumer<T1, T1> mergeConsumer) {
        return new MergeHandler<T1>(mergeConsumer);
    }
}
