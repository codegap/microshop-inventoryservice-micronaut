package com.codegap.microshop.inventory.dto.impl;

import com.codegap.microshop.inventory.dto.PagedListDto;
import com.codegap.microshop.inventory.model.impl.Brand;

public class BrandPagedListDto extends PagedListDto<Brand> {
}
