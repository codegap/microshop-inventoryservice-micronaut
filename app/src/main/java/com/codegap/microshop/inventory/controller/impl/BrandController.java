package com.codegap.microshop.inventory.controller.impl;

import com.codegap.microshop.inventory.controller.ApiUri;
import com.codegap.microshop.inventory.controller.PageableCrudController;
import com.codegap.microshop.inventory.dto.impl.BrandDetailsDto;
import com.codegap.microshop.inventory.model.impl.Brand;
import com.codegap.microshop.inventory.service.impl.BrandService;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.validation.Validated;

@Validated
@Controller(ApiUri.BRAND_V1)
public class BrandController extends PageableCrudController<Brand> {

    public BrandController(BrandService service) {
        super(service, ApiUri.BRAND_V1, Brand.SORT);
    }

    @Get(ApiUri.ID + ApiUri.VIEW_DETAILS)
    public HttpResponse<BrandDetailsDto> findDetailsDtoById(@PathVariable Long id) {
        return ((BrandService)getService()).findDetailsDtoById(id)
                .map(this::ok)
                .orElseGet(this::notFound);
    }
}
