package com.codegap.microshop.inventory.service;

import com.codegap.microshop.inventory.model.IdentifiableEntity;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.repository.PageableRepository;
import lombok.Getter;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public abstract class PageableCrudService<T extends IdentifiableEntity> {

    @Getter
    private final PageableRepository<T, Long> repository;
    @Getter
    private final MergeHandler<T> mergeHandler;
    @Getter
    private final PageableSearchHandler<T> searchHandler;

    protected PageableCrudService(PageableRepository<T, Long> repository,
                                  MergeHandler<T> mergeHandler,
                                  PageableSearchHandler<T> searchHandler) {
        this.repository = repository;
        this.mergeHandler = mergeHandler;
        this.searchHandler = searchHandler;
    }

    /**
     * Performs a search as per the configured searchQuery, or delegates to this::findAll if not.
     */
    public Page<T> search(String searchQuery, Pageable pageable) {
        return getSearchHandler().handleOrElse(searchQuery, pageable, this::findAll);
    }

    public Page<T> findAll(Pageable pageable) {
        return getRepository().findAll(pageable);
    }

    public Optional<T> findById(Long id) {
        return getRepository().findById(id);
    }

    public T insert(T t) {
        return getRepository().save(t.setId(null)); // making sure it is inserted as a new record
    }

    @Transactional // all or nothing
    public void insertAll(List<T> list) {
        list.parallelStream().forEach(t -> getRepository().save(t.setId(null)));
    }

    @Transactional
    public Optional<T> update(Long id, T merge) {
        return getRepository().findById(id)
                .map(e -> {
                    getMergeHandler().handle(e, merge);
                    return getRepository().update(e);
                });
    }

    public void deleteById(Long id) {
        getRepository().deleteById(id);
    }

    @Transactional // all or nothing
    public void deleteAll(List<Long> ids) {
        ids.parallelStream().forEach(getRepository()::deleteById);
    }

    public void deleteAll() {
        getRepository().deleteAll();
    }
}
