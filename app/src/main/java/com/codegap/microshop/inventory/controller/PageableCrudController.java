package com.codegap.microshop.inventory.controller;

import com.codegap.microshop.inventory.dto.PagedListDto;
import com.codegap.microshop.inventory.model.IdentifiableEntity;
import com.codegap.microshop.inventory.service.PageableCrudService;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.model.Sort;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import lombok.Getter;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

public abstract class PageableCrudController<T extends IdentifiableEntity> {
    public static final String DEFAULT_PAGE = "1";
    public static final String DEFAULT_PAGESIZE = "5";

    @Getter
    private final PageableCrudService<T> service;
    @Getter
    private final String baseUrl;
    @Getter
    private final Sort defaultSort;

    public PageableCrudController(PageableCrudService<T> service, String baseUrl, Sort defaultSort) {
        this.service = service;
        this.baseUrl = baseUrl;
        this.defaultSort = defaultSort;
    }

    protected <R> HttpResponse<R> ok(R r) {
        return HttpResponse.ok(r);
    }

    protected <R> HttpResponse<R> notFound() {
        return HttpResponse.notFound();
    }

    protected HttpResponse<T> created(T t) {
        return created(t, toIdentityLocation(t));
    }

    protected HttpResponse<T> created(T t, URI location) {
        return HttpResponse.created(t, location);
    }

    protected <R> HttpResponse<R> noContent() {
        return HttpResponse.noContent();
    }

    protected <R> HttpResponse<R> accepted() {
        return accepted(URI.create(ensureTrailingSlash(getBaseUrl())));
    }

    protected <R> HttpResponse<R> accepted(URI location) {
        return HttpResponse.accepted(location);
    }

    protected URI toIdentityLocation(T t) {
        return URI.create(ensureTrailingSlash(getBaseUrl()) + t.getId());
    }

    private String ensureTrailingSlash(String s) {
        return s.endsWith("/") ? s : s + '/';
    }

    protected Pageable toPageable(int page, int size) {
        return toPageable(page, size, getDefaultSort());
    }

    protected Pageable toPageable(int page, int size, Sort sort) {
        page = Math.max(page, 1) - 1;  // page-number is 1-based in the UI
        size = Math.max(size, 1);
        return Pageable.from(page, size, sort);
    }

    //// Generic CRUD endpoints ////
    @Get("/{?search,page,size}")
    public HttpResponse<PagedListDto<T>> findAll(
            @QueryValue @Nullable String search,
            @QueryValue(defaultValue = DEFAULT_PAGE) int page,
            @QueryValue(defaultValue = DEFAULT_PAGESIZE) int size) {

        Pageable pageable = toPageable(page, size);

        Page<T> result;
        if (search != null && !search.isBlank()) {
            result = getService().search(search, pageable);
        } else {
            result = getService().findAll(pageable);
        }

        return ok(PagedListDto.<T>of(result));
    }

    @Get(ApiUri.ID)
    public HttpResponse<T> findById(@PathVariable(ApiUri.PathVariable.ID) Long id) {
        return getService().findById(id)
                .map(this::ok)
                .orElseGet(this::notFound);
    }

    @Post()
    public HttpResponse<T> insert(@Body @Valid T e) {
        e.setId(null); // make sure it is saved as a new record
        e = getService().insert(e);
        return created(e);
    }

    @Post(ApiUri.BATCH)
    public HttpResponse<Void> insertAll(@Body List<@Valid T> list) {
        getService().insertAll(list);
        return accepted();
    }

    @Put(ApiUri.ID)
    public HttpResponse<T> update(@PathVariable(ApiUri.PathVariable.ID) Long id, @Body @Valid T e) {
        return getService().update(id, e)
                .map(this::ok)
                .orElseGet(this::notFound);
    }

    @Delete(ApiUri.ID)
    public HttpResponse<Void> delete(@PathVariable(ApiUri.PathVariable.ID) Long id) {
        getService().deleteById(id);
        return noContent();
    }

    @Delete(ApiUri.BATCH)
    public HttpResponse<Void> deleteAll(@Body List<@Valid Long> ids) {
        getService().deleteAll(ids);
        return accepted();
    }

    @Delete // no postfix; operating on the whole collection
    public HttpResponse<Void> deleteAll() {
        getService().deleteAll();
        return accepted();
    }
}
