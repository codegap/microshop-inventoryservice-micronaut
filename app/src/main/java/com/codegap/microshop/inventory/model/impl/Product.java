package com.codegap.microshop.inventory.model.impl;

import com.codegap.microshop.inventory.model.IdentifiableEntity;
import io.micronaut.data.model.Sort;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors(chain = true)
@Builder(toBuilder = true)
public class Product implements IdentifiableEntity {
    public static final Sort SORT = Sort.of(
            Sort.Order.asc("name"),
            Sort.Order.asc("brand")
    );

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @ToString.Exclude
    private Brand brand;

    @NotBlank
    @Size(min = 1, max = 100)
    private String name;

    @Size(max = 255)
    private String description;

    @NotNull
    @Min(0)
    private BigDecimal price;

    @NotNull
    @Min(0)
    private int stock;

    // auto-populated in database
    private LocalDateTime dateCreated;
    // auto-populated in database & updated via trigger
    private LocalDateTime dateUpdated;


    public Product(Brand brand, String name, String description, BigDecimal price, int stock) {
        this.brand = brand;
        this.name = name;
        this.description = description;
        this.price = price;
        this.stock = stock;
    }
}
