package com.codegap.microshop.inventory.controller.impl;

import com.codegap.microshop.inventory.controller.ApiUri;
import com.codegap.microshop.inventory.controller.PageableCrudController;
import com.codegap.microshop.inventory.model.impl.Product;
import io.micronaut.http.annotation.Controller;
import io.micronaut.validation.Validated;

@Validated
@Controller(ApiUri.PRODUCT_V1)
public class ProductController extends PageableCrudController<Product> {

    public ProductController() {
        super(null /*fixme*/, ApiUri.PRODUCT_V1, Product.SORT);
    }
}
