package com.codegap.microshop.inventory.service.impl;

import com.codegap.microshop.inventory.dao.impl.BrandRepository;
import com.codegap.microshop.inventory.dao.impl.ProductRepository;
import com.codegap.microshop.inventory.dto.impl.BrandDetailsDto;
import com.codegap.microshop.inventory.model.impl.Brand;
import com.codegap.microshop.inventory.service.MergeHandler;
import com.codegap.microshop.inventory.service.PageableCrudService;
import com.codegap.microshop.inventory.service.PageableSearchHandler;
import jakarta.inject.Singleton;

import java.util.Optional;

@Singleton
public class BrandService extends PageableCrudService<Brand> {
    private final ProductRepository productRepository;

    public BrandService(BrandRepository repository, ProductRepository productRepository) {
        super(repository,
                MergeHandler.of(Brand::merge),
                PageableSearchHandler.of(repository::findAllByNameContains));
        this.productRepository = productRepository;
    }

    public Optional<BrandDetailsDto> findDetailsDtoById(Long id) {
        return getRepository().findById(id)
                .map(this::createDetailsDto);
    }

    private BrandDetailsDto createDetailsDto(Brand brand) {
        int count = productRepository.countByBrand(brand);
        return BrandDetailsDto.of(brand, count);
    }
}
