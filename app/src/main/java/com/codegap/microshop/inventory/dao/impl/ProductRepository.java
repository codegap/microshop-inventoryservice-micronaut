package com.codegap.microshop.inventory.dao.impl;

import com.codegap.microshop.inventory.model.impl.Brand;
import com.codegap.microshop.inventory.model.impl.Product;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.repository.PageableRepository;

@Repository
public interface ProductRepository extends PageableRepository<Product, Long> {
    // @Join(value = "brand", type = Join.Type.LEFT_FETCH)
    Page<Product> findAllByNameContainsOrDescriptionContains(String search, String sameSearch, Pageable pageable);
    int countByBrand(Brand brand);
    Page<Product> findAllByBrand(Brand brand, Pageable pageable);
}
