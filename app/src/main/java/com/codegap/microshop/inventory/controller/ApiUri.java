package com.codegap.microshop.inventory.controller;

import com.codegap.microshop.inventory.model.IdentifiableEntity;

public final class ApiUri {
    static final class PathVariable {
        public static final String ID = "id";
    }

    public static final String API_PREFIX = "/api";

    public static final String ID = "/{" + PathVariable.ID + "}";
    public static final String VIEW_DETAILS = "/details";

    public static final String BATCH = "/batch";

    public static final String BRAND_V1 = API_PREFIX + "/brand/v1";
    public static final String PRODUCT_V1 = API_PREFIX + "/product/v1";

    public static <T extends IdentifiableEntity> String getIdentityUri(String collectionUri, T t) {
        return getIdentityUri(collectionUri, t.getId());
    }

    public static String getIdentityUri(String collectionUri, Long id) {
        return collectionUri + "/" + id;
    }

    public static String getBatchUri(String collectionUri) {
        return collectionUri + ApiUri.BATCH;
    }
}
