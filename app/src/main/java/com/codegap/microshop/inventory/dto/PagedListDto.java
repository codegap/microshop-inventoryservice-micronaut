package com.codegap.microshop.inventory.dto;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.data.model.Page;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Introspected
public class PagedListDto<T> extends ListDto<T> {

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PagedListMeta {
        long items = 0;
        int pages = 0;
        int current = 1;
    }

    PagedListMeta meta;

    public PagedListDto(List<T> content, PagedListMeta meta) {
        this.content = content;
        this.meta = meta;
    }

    public static <T> PagedListDto<T> of(Page<T> page) {
        if (page != null) {
            return new PagedListDto<>(page.getContent(), new PagedListMeta(
                    page.getTotalSize(),
                    page.getTotalPages(),
                    page.getPageNumber() + 1 // page-number is 1-based in the UI
            ));
        } else {
            return new PagedListDto<>(List.of(), new PagedListMeta());
        }
    }
}
