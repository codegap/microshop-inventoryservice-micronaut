package com.codegap.microshop.inventory.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ListDto<T> extends Dto<T> {
    public List<T> content;
}
