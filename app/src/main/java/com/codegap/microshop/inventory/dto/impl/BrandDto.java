package com.codegap.microshop.inventory.dto.impl;

import com.codegap.microshop.inventory.dto.Dto;
import com.codegap.microshop.inventory.model.impl.Brand;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
public class BrandDto extends Dto<Brand> {
    private Long id;
    private String name;

    public static BrandDto of(Brand brand) {
        return BrandDto.builder()
                .id(brand.getId())
                .name(brand.getName())
                .build();
    }
}
