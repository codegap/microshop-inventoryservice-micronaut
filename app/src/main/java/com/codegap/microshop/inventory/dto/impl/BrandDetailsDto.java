package com.codegap.microshop.inventory.dto.impl;

import com.codegap.microshop.inventory.model.impl.Brand;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
public class BrandDetailsDto extends BrandDto {
    private Integer productCount;

    public static BrandDetailsDto of(Brand brand, Integer productCount) {
        return BrandDetailsDto.builder()
                .id(brand.getId())
                .name(brand.getName())
                .productCount(productCount)
                .build();
    }
}