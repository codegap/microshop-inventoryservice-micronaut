create table brand(
    id bigint auto_increment,
    name varchar(100) not null,

    constraint brand_pk primary key (id)
);

create table product(
    id bigint auto_increment,
    brand_id bigint not null,
    name varchar(100) not null,
    description varchar(255) default null,
    price decimal(5,2) not null,
    stock int not null,
    date_created datetime not null default current_timestamp,
    date_updated datetime not null default current_timestamp on update current_timestamp,

    index product_brand_ix (brand_id),
    constraint product_pk primary key (id),
    constraint product_brand_fk foreign key (brand_id) references brand(id)
)